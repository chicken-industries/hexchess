// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HexChessGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HEXCHESS_API AHexChessGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
