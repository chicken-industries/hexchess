#pragma once

#include "CoreMinimal.h"
#include "HexagonGrid.h"
#include "HexChessFigure.h"
#include "HexChessGrid.generated.h"

UCLASS()
class HEXCHESS_API AHexChessGrid : public AHexagonGrid
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	class UProceduralMeshComponent* ChessBoardMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(EditFixedSize), Category = HexChess)
	TArray<FLinearColor> TileColors
	{
		{0.3f, 0.25f, 0.1f, 1.f},
		{0.1f, 0.03f, 0.01f, 1.f},
		{0.3f, 0.1f, 0.02f, 1.f},
	};
	
	AHexChessGrid();

protected:
	void InitTilesArray() override;

private:
	void ColorTiles();
	void ColorTilesSingleColor(FIntPoint const& StartingTile, int MeshIndex);

	UFUNCTION(BlueprintPure)
	TArray<FIntPoint> GetPossibleMoveTargets(FIntPoint const& StartingTile, EHexChessFigureType FigureType, int PlayerId) const;
	
	UFUNCTION(BlueprintPure)
	TArray<FIntPoint> GetPossibleStrikeTargets(FIntPoint const& StartingTile, EHexChessFigureType FigureType, int PlayerId) const;

	void AddAllUnblockedTilesInDirection(TArray<FIntPoint>& PossibleTiles, FIntPoint const& StartingTile, FIntPoint const& Direction, int MaxDistance = 20) const;
	void AddFirstEnemyTileInDirection(TArray<FIntPoint>& PossibleTiles, FIntPoint const& StartingTile, FIntPoint const& Direction, int PlayerId, int MaxDistance = 20) const;

	static TArray<FIntPoint> GetMoveDirectionsRook();
	static TArray<FIntPoint> GetMoveDirectionsBishop();
	static TArray<FIntPoint> GetMoveDirectionsQueen();
	static TArray<FIntPoint> GetMoveDirectionsKing();
	static TArray<FIntPoint> GetMoveDirectionsKnight();
	static TArray<FIntPoint> GetMoveDirectionsPawn(int PlayerId);
	static TArray<FIntPoint> GetStrikeDirectionsPawn(int PlayerId);

	UFUNCTION(BlueprintPure)
	static TArray<FIntPoint> GetPawnOriginTiles(int PlayerId);
};
