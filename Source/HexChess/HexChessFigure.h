#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HexChessFigure.generated.h"

UENUM(BlueprintType)
enum class EHexChessFigureType : uint8
{
	Rook UMETA(DisplayName = "Rook"),
	King UMETA(DisplayName = "King"),
	Queen UMETA(DisplayName = "Queen"),
	Bishop UMETA(DisplayName = "Bishop"),
	Knight UMETA(DisplayName = "Knight"),
	Pawn UMETA(DisplayName = "Pawn"),
};


UCLASS()
class HEXCHESS_API AHexChessFigure : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (ExposeOnSpawn=true, ClampMin = 0, ClampMax = 1))
	int PlayerId = 0;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FIntPoint Tile {0, 0};
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Meta = (ExposeOnSpawn=true))
	EHexChessFigureType Type = EHexChessFigureType::Rook;
};
