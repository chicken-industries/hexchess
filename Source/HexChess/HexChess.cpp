// Copyright Epic Games, Inc. All Rights Reserved.

#include "HexChess.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HexChess, "HexChess" );
