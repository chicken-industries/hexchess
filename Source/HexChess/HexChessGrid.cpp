#include "HexChessGrid.h"

#include "HexChessFigure.h"
#include "ProceduralMeshComponent.h"

AHexChessGrid::AHexChessGrid()
{
	NumTiles2D = {11, 11};
	StartingCoords = {-3, -5}; // set (0,0) in the center

	ChessBoardMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("ChessBoardMesh"));
	ChessBoardMesh->SetupAttachment(RootComponent);
}

void AHexChessGrid::InitTilesArray()
{
	Super::InitTilesArray();

	for (auto const& Coords : GetTilesInRing({0, 0}, 5, 11))
		SetTileState(Coords, ETileState::Disabled);

	ColorTiles();
}

void AHexChessGrid::ColorTiles()
{
	ColorTilesSingleColor({0, 0}, 0);
	ColorTilesSingleColor({1, 0}, 1);
	ColorTilesSingleColor({0, 1}, 2);
}

void AHexChessGrid::ColorTilesSingleColor(FIntPoint const& StartingTile, int MeshIndex)
{
	TArray<FVector> Vertices;
	TArray<int> Triangles;

	TArray<FIntPoint> Visited;
	TArray<FIntPoint> Queue;
	Queue.Add(StartingTile);

	while (Queue.Num() > 0)
	{
		auto NewTile = Queue.Pop();

		if (!Visited.Contains(NewTile))
		{
			Visited.Add(NewTile);

			for (FIntPoint DiagonalNeighbour : GetDiagonalNeighbours(NewTile))
				Queue.Add(DiagonalNeighbour);
		}
	}

	for (auto Coords : Visited)
		CalcTileTriangulation(GetStorageIndexFromTileCoords(Coords), Vertices, Triangles);

	ChessBoardMesh->CreateMeshSection_LinearColor(MeshIndex, Vertices, Triangles, {}, {}, {}, {}, false);
}

TArray<FIntPoint> AHexChessGrid::GetPossibleMoveTargets(FIntPoint const& StartingTile, EHexChessFigureType FigureType, int PlayerId) const
{
	TArray<FIntPoint> ValidMoveTargets;
	
	switch (FigureType)
	{
		case EHexChessFigureType::Rook:
			for (auto const& Direction : GetMoveDirectionsRook())
				AddAllUnblockedTilesInDirection(ValidMoveTargets, StartingTile, Direction);
		break;
		
		case EHexChessFigureType::Bishop:
			for (auto const& Direction : GetMoveDirectionsBishop())
				AddAllUnblockedTilesInDirection(ValidMoveTargets, StartingTile, Direction);
		break;
		
		case EHexChessFigureType::Queen:
			for (auto const& Direction : GetMoveDirectionsQueen())
				AddAllUnblockedTilesInDirection(ValidMoveTargets, StartingTile, Direction);
		break;
				
		case EHexChessFigureType::King:
			for (auto const& Direction : GetMoveDirectionsKing())
				AddAllUnblockedTilesInDirection(ValidMoveTargets, StartingTile, Direction, 1);
		break;
							
		case EHexChessFigureType::Knight:
			for (auto const& Direction : GetMoveDirectionsKnight())
				AddAllUnblockedTilesInDirection(ValidMoveTargets, StartingTile, Direction, 1);
		break;

		case EHexChessFigureType::Pawn:
			for (auto const& Direction : GetMoveDirectionsPawn(PlayerId))
			{
				auto TargetTile = StartingTile + Direction;

				if (!IsTileBlocked(TargetTile))
				{
					ValidMoveTargets.Add(TargetTile);

					if (bool PawnIsOnOriginTile = GetPawnOriginTiles(PlayerId).Contains(StartingTile))
					{
						auto DoubleMoveTile = StartingTile + Direction * 2;

						if (!IsTileBlocked(DoubleMoveTile))
							ValidMoveTargets.Add(DoubleMoveTile);
					}
				}
			}
		break;
		
	}

	return ValidMoveTargets;
}

TArray<FIntPoint> AHexChessGrid::GetPossibleStrikeTargets(FIntPoint const& StartingTile, EHexChessFigureType FigureType, int PlayerId) const
{
	TArray<FIntPoint> ValidStrikeTargets;
	
	switch (FigureType)
	{
		case EHexChessFigureType::Rook:
			for (auto const& Direction : GetMoveDirectionsRook())
				AddFirstEnemyTileInDirection(ValidStrikeTargets, StartingTile, Direction, PlayerId);
		break;
		
		case EHexChessFigureType::Bishop:
			for (auto const& Direction : GetMoveDirectionsBishop())
				AddFirstEnemyTileInDirection(ValidStrikeTargets, StartingTile, Direction, PlayerId);
		break;
		
		case EHexChessFigureType::Queen:
			for (auto const& Direction : GetMoveDirectionsQueen())
				AddFirstEnemyTileInDirection(ValidStrikeTargets, StartingTile, Direction, PlayerId);
		break;
				
		case EHexChessFigureType::King:
			for (auto const& Direction : GetMoveDirectionsKing())
				AddFirstEnemyTileInDirection(ValidStrikeTargets, StartingTile, Direction, PlayerId, 1);
		break;
							
		case EHexChessFigureType::Knight:
			for (auto const& Direction : GetMoveDirectionsKnight())
				AddFirstEnemyTileInDirection(ValidStrikeTargets, StartingTile, Direction, PlayerId, 1);
		break;

		case EHexChessFigureType::Pawn:
			for (auto const& Direction : GetStrikeDirectionsPawn(PlayerId))
				AddFirstEnemyTileInDirection(ValidStrikeTargets, StartingTile, Direction, PlayerId, 1);
		break;
		
	}

	return ValidStrikeTargets;
}

void AHexChessGrid::AddFirstEnemyTileInDirection(TArray<FIntPoint>& PossibleTiles, FIntPoint const& StartingTile, FIntPoint const& Direction, int PlayerId, int MaxDistance) const
{
	int Distance = 1;
				
	while (true)
	{
		FIntPoint TargetTile = StartingTile + Direction * Distance;

		if (GetTileState(TargetTile) == ETileState::Disabled)
			break;

		if (auto Figure = dynamic_cast<AHexChessFigure*>(GetActorOnTile(TargetTile)))
		{
			if (Figure->PlayerId != PlayerId)
				PossibleTiles.Add(TargetTile);
			
			break;
		}
		
		if (Distance >= MaxDistance)
			break;

		++Distance;
	}
}

void AHexChessGrid::AddAllUnblockedTilesInDirection(TArray<FIntPoint>& PossibleTiles, FIntPoint const& StartingTile, FIntPoint const& Direction, int MaxDistance) const
{
	int Distance = 1;
				
	while (true)
	{
		FIntPoint TargetTile = StartingTile + Direction * Distance;

		if (GetTileState(TargetTile) == ETileState::Disabled || IsTileBlocked(TargetTile))
			break;
		
		PossibleTiles.Add(TargetTile);

		if (Distance >= MaxDistance)
			break;

		++Distance;
	}
}

TArray<FIntPoint> AHexChessGrid::GetMoveDirectionsRook()
{
	return {{1, 0}, {0, 1}, {-1, 0}, {0, -1}, {1, -1}, {-1, 1}};
}

TArray<FIntPoint> AHexChessGrid::GetMoveDirectionsBishop()
{
	return {{1, 1}, {-1, 2}, {-2, 1}, {-1, -1}, {1, -2}, {2, -1}};
}

TArray<FIntPoint> AHexChessGrid::GetMoveDirectionsQueen()
{
	TArray<FIntPoint> MoveDirections = GetMoveDirectionsRook();
	MoveDirections.Append(GetMoveDirectionsBishop());
	return MoveDirections;
}

TArray<FIntPoint> AHexChessGrid::GetMoveDirectionsKing()
{
	return GetMoveDirectionsQueen();
}

TArray<FIntPoint> AHexChessGrid::GetMoveDirectionsKnight()
{
	return{
		{2, 1},
		{1, 2},
		{-1, 3},
		{-2, 3},
		{-3, 2},
		{-3, 1},
		{-2, -1},
		{-1, -2},
		{1, -3},
		{2, -3},
		{3, -2},
		{3, -1},
	};
}

TArray<FIntPoint> AHexChessGrid::GetMoveDirectionsPawn(int PlayerId)
{
	if (PlayerId == 0)
		return {{1, 0}};

	return {{-1, 0}};
}

TArray<FIntPoint> AHexChessGrid::GetStrikeDirectionsPawn(int PlayerId)
{
	if (PlayerId == 0)
		return {{0, 1}, {1, -1}};
	
	return {{0, -1}, {-1, 1}};
}

TArray<FIntPoint> AHexChessGrid::GetPawnOriginTiles(int PlayerId)
{
	TArray<FIntPoint> PawnOrigins;
	
	if (PlayerId == 0)
	{
		for (int i = -4; i <= 0; ++i)
			PawnOrigins.Add({-1, i});

		for (int i = 1; i <=4; ++i)
			PawnOrigins.Add({-i-1, i});
	}
	else if (PlayerId == 1)
	{
		for (int i = 0; i <= 4; ++i)
			PawnOrigins.Add({1, i});

		for (int i = 1; i <=4; ++i)
			PawnOrigins.Add({i+1, -i});
	}

	return PawnOrigins;
}
